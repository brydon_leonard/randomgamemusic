var vidList = [];
$(document).ready(function(){
	var xmlReq = new XMLHttpRequest();
	xmlReq.open('GET', 'ostXML.xml', true);
	xmlReq.send();
	xmlReq.onreadystatechange = function()
	{
		if (xmlReq.readyState == 4 && xmlReq.status == 200)
		{
			var xmlDoc = xmlReq.responseXML;
			var nodeList = xmlDoc.getElementsByTagName('track');
			for (var i = 0; i < nodeList.length; i++)
			{
				vidList.push(nodeList[i].getAttribute('vidURL'));
			}
			
			var randURL = getRandomVideoURL();
			var $frame = $('<iframe class="embed-responsive-item large" src="'+randURL+'?autoplay=1" style="position:absolute;width:100%;height:100%"></iframe>');
			console.log(randURL);
			$('#holder').append($frame);
		}
	}
});

getRandomVideoURL = function()
{
	rand = Math.random() * vidList.length;
	return vidList[Math.round(rand)];
}
